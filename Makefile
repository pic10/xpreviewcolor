CC=gcc
LDFLAGS=-L/usr/include/X11 -lX11
CFLAGS=-W -Wall -Werror -Wextra -pedantic -pedantic-errors -ansi
INSTALL_DIR=/usr/bin
SOURCES=xpreviewcolor.c
BINARIES=xpreviewcolor
STRIP_PROGRAM=strip

${BINARIES}: ${SOURCES}
	${CC} ${SOURCES} ${LDFLAGS} ${CFLAGS} -o ${BINARIES}

clean:
	rm -f ${BINARIES}

install:
	install -s --strip-program ${STRIP_PROGRAM} ${BINARIES} ${INSTALL_DIR}

uninstall:
	rm -f ${INSTALL_DIR}/${BINARIES}
