/* https://creativecommons.org/publicdomain/zero/1.0/deed */
/* <pic10@airmail.cc> */

#include <X11/Xlib.h>

unsigned long get_color(Display *d, int s, char *color) {
	XColor tmp;
	Colormap cm = DefaultColormap(d, s);
	XParseColor(d, cm, color, &tmp);
	XAllocColor(d, cm, &tmp);
	return tmp.pixel;
}

int main(int argc, char **argv) {
	Display *d;
	Window w;
	XEvent e;
	int s;
	Atom wm;

	(void)argc;

	if((d = XOpenDisplay(NULL)) == NULL)
		return 1;

	s = DefaultScreen(d);
	w = XCreateSimpleWindow(d, RootWindow(d, s), 10, 10, 300, 300, 1, BlackPixel(d, s), get_color(d, s, argv[1]));
	wm = XInternAtom(d, "WM_DELETE_WINDOW", True); /* need this to gracefully exit the application */
	XSetWMProtocols(d, w, &wm, 1);
	XStoreName(d, w, "xpreviewcolor");
	XMapWindow(d, w);

	while(1) {
		XNextEvent(d, &e);
		if(e.type == ClientMessage)
			break;
	}

	XCloseDisplay(d);
	return 0;
}
